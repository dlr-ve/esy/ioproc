# To build the dist package execute:

```poetry build```

# To install with pip
```pip install dist\ioproc-2.0.9-py3-none-any.whl --force --no-deps```


# Uploading to pip cheese shop:
(Test pypi repo!)

```python -m twine upload --repository testpypi dist/*```

# installing from dlr internal package repo:

```pip install ioproc --index-url https://__token__:NLwkYL2DwyvTnF82rVyx@gitlab.dlr.de/api/v4/projects/4186/packages/pypi/simple --force-reinstall --no-deps```

# publishing to the dlr internal package repo:

```poetry publish --repository ioproc```

