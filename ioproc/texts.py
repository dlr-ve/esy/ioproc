
__author__ = ["Benjamin Fuchs"]
__copyright__ = "Copyright 2022, German Aerospace Center (DLR)"
__credits__ = [
    "Felix Nitsch",
    "Jan Buschmann",
]

__license__ = "MIT"
__maintainer__ = "Benjamin Fuchs"
__email__ = "ioProc@dlr.de"
__status__ = "Production"

welcome_line_1 = r" _      __    __                     __         __  __         _                                                         ____               _                    __"
welcome_line_2 = r"| | /| / /__ / /______  __ _  ___   / /____    / /_/ /  ___   (_)__  ___  _______  ____  ___ ____  ___    _______  ___  / _(_)__ _  _    __(_)_____ ___ ________/ /"
welcome_line_3 = r"| |/ |/ / -_) / __/ _ \/  ' \/ -_) / __/ _ \  / __/ _ \/ -_) / / _ \/ _ \/ __/ _ \/ __/ / _ `/ _ \/ _ \  / __/ _ \/ _ \/ _/ / _ `/ | |/|/ / /_ /_ // _ `/ __/ _  / "
welcome_line_4 = r"|__/|__/\__/_/\__/\___/_/_/_/\__/  \__/\___/  \__/_//_/\__/ /_/\___/ .__/_/  \___/\__/  \_,_/ .__/ .__/  \__/\___/_//_/_//_/\_, /  |__,__/_//__/__/\_,_/_/  \_,_/  "
welcome_line_5 = r"                                                                   /_/                      /_/  /_/                        /___/                                  "

quit_instruction = 'To quit at any time, type "q!"'
input_instruction = 'Please answer the following questions to {what_to_do} the ioproc configuration\nat "{filepath}".'
header_user_info = 'User specific information:'

check_info = 'Thank you! Please check the provided information:'

store_prompt = 'Do ypu want to store the given information to disk? [yY/nN] '
store_location = 'The provided information is stored in your home drive at "{filepath}".'
discard_message = 'Configuration changes are now discarded.'

header_meta_info = 'Meta data specific information:'

goodbye_line_1 = r"                     ____                       __  __                               _              __          __"
goodbye_line_2 = r"  ___ ____  ___  ___/ / /  __ __  ___ ____  ___/ / / /  ___ __  _____   ___ _  ___  (_)______   ___/ /__ ___ __/ /"
goodbye_line_3 = r" / _ `/ _ \/ _ \/ _  / _ \/ // / / _ `/ _ \/ _  / / _ \/ _ `/ |/ / -_) / _ `/ / _ \/ / __/ -_) / _  / _ `/ // /_/ "
goodbye_line_4 = r" \_, /\___/\___/\_,_/_.__/\_, /  \_,_/_//_/\_,_/ /_//_/\_,_/|___/\__/  \_,_/ /_//_/_/\__/\__/  \_,_/\_,_/\_, (_)  "
goodbye_line_5 = r"/___/                    /___/                                                                          /___/     "